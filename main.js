// 1. Прототипне наслідування працює шляхом створення одного об'єкту з іншого об'єкту. 
// Тобто новому об'єкту передаються властивості та методи узагальненого
// об'єкта, що наслідується (прототипа). Пошук починається від "спадкоємців" і
// в разі відсутності вказаних властивостей та методів, піднімається вгору
// по ієрархії, закінчуючи прототипом.

// 2. super() - функція, яка викликає батьківський конструктор, необхідна для
// успадкування та ініціалізації батьківських властивостей для того, щоб
// не повторювати частини конструктора, що є однаковими як для батьківського,
// так і дочірнього класу.



class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
       return this._name;
    }

    set name(newName){
        this._name = newName;
    }


    get age(){
        return this._age;
    }

    set age(newAge){
        this._age = newAge;
    }


    get salary(){
        return this._salary;
    }

    set salary(newSalary){
        this._salary = newSalary;
    }
}


class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this._lang = lang
    }

    get salary(){
        return this._salary * 3; 
    }
}


const programmer = new Programmer ("Larysa", 29, 20000, "JavaScript")
const programmer2 = new Programmer ("Dmytro", 35, 37500, "C#")

console.log(programmer)
console.log(programmer2)



